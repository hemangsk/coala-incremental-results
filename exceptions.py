class UnknownBearError(Exception):
    """
    Raised when an unknown bear is used.
    """
    pass

class InsufficientSettingsError(Exception):
    """
    Raised when a mandatory setting is not provided.
    """
    pass
