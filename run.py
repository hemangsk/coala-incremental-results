#!/usr/bin/env python3

"""
Runs coala-quickstart and coala in a container to return coafile
skeleton and file dicts and results respectively.
"""
import json
import logging
import os
import shutil
import subprocess
import sys
import tempfile

from coalib.misc.Shell import run_shell_command

from exceptions import InsufficientSettingsError
from exceptions import UnknownBearError
from utilities import coafile_to_json
from utilities import create_temp_dir_and_file
from utilities import get_language_extension
from utilities import read_coafile
from utilities import sections_to_command
from utilities import TEMP_DIR
from utilities import write_to_file

def main():
    """
    Triggers code analysis or Returns coafile skeleton based on running mode.
    """
    request = json.loads(sys.argv[1])
    mode = request["mode"]

    file_content = request.get("file_content", None)
    language = request.get("language", None)
    url = request.get("url", None)
    sections = request.get("sections", None)

    if mode == "bears":

        if file_content:

            extension = get_language_extension(language)
            directory_name, filename = create_temp_dir_and_file(
                extension, TEMP_DIR)
            write_to_file(filename, file_content)

        elif url:

            directory_name = tempfile.mkdtemp(dir=TEMP_DIR)
            subprocess.run(['git', 'clone', '--depth=1', url],
                           cwd=directory_name)

        cmd = ["coala-quickstart", "--ci", "--allow-incomplete-sections"]
        run_shell_command(cmd, cwd=directory_name)
        response = json.dumps({
            "response": coafile_to_json(directory_name),
            "coafile": read_coafile(directory_name + os.sep + '.coafile')
        })

    elif mode == "coala":

        if file_content:

            extension = get_language_extension(language)
            directory_name, filename = create_temp_dir_and_file(extension)
            write_to_file(filename, file_content)

        elif url:

            directory_name = tempfile.mkdtemp(dir=TEMP_DIR)
            subprocess.run(['git', 'clone', '--depth=1', url],
                           cwd=directory_name)

        cmd = ["coala", "--json", "-S"]
        try:
            cmd = sections_to_command(sections, cmd)
        except (UnknownBearError, InsufficientSettingsError) as exc:
            logging.exception("An error occured: ")
            print(json.dumps({"error": str(exc)}))
            return
        cmd.append("--save")
        cmd = " ".join(cmd)

        shell_output, shell_err = run_shell_command(cmd, cwd=directory_name)
        response = json.dumps({
            "response": json.loads(shell_output),
            "coafile": read_coafile(directory_name + os.sep + '.coafile')
        })

    print(response)
    shutil.rmtree(directory_name)


if __name__ == '__main__':
    main()
